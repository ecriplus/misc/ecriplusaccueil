#{ pkgs ? import <nixpkgs> }:
#  pkgs.mkShell {
#    # nativeBuildInputs is usually what you want -- tools you need to run
#    nativeBuildInputs = [ pkgs.nodejs ];
#}
with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "node";
    buildInputs = [
        jq
        nodejs
        vips
        hugo
        awscli2
    ];
    shellHook = ''
        export PATH="$PWD/node_modules/.bin/:$PATH"
        alias scripts='jq ".scripts" package.json'
    '';
}
