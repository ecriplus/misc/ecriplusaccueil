FROM docker.io/hugomods/hugo:exts
# ENV NODE_ENV="production" \
#     HUGO_BASEURL="https://tests.ecriplus.fr" \
#     HUGO_PARAMS_BaseDomain="tests.ecriplus.fr" \
#     HUGO_TITLE="Instance de tests" \
#     HUGO_PARAMS_seeds=""

RUN apk add --no-cache --virtual .build-deps curl \
 && curl -OL "https://github.com/ajgon/envdir/releases/download/v0.1.2/envdir_$(uname -s)_$(uname -m).tar.gz" \
 && tar -xzf "envdir_$(uname -s)_$(uname -m).tar.gz" -C /usr/bin/ envdir \
 && rm -rf "envdir_$(uname -s)_$(uname -m).tar.gz" \
 && apk del .build-deps

RUN apk add --no-cache caddy aws-cli

COPY <<EOF /etc/caddy/Caddyfile
:80 {
 # Set this path to your site''s directory.
 root * /public

 # Enable the static file server.
 file_server
}
EOF

COPY --chmod=755 <<EOF /usr/local/sbin/check-seeds.sh
#!/usr/bin/env sh
seed_path="/tmp/seeds.json"
sum_path="\${seed_path}.sum"

if [ -z "\$AWS_SECRET_ACCESS_KEY" ] || \
   [ -z "\$AWS_ACCESS_KEY_ID" ] || \
   [ -z "\$AWS_DEFAULT_REGION" ] || \
   [ -z "\$AWS_ENDPOINT" ] || \
   [ -z "\$S3_SEEDS_URL" ]; then
  echo "S3 env not set, checking /env/HUGO_PARAMS_seeds"
  seed_path="/env/HUGO_PARAMS_seeds"
  sum_path="/tmp/seeds.sum"
else
  echo "S3 env set, fetching seeds def"
  if ! (aws --endpoint-url "\$AWS_ENDPOINT" s3 cp "\${S3_SEEDS_URL}" "\$seed_path"); then
    if [ -f "\$seed_path"]; then
      rm "\$seed_path"
    fi
    touch "\$seed_path"
  fi
fi

if [ ! -f "\${seed_path}" ]; then
  echo "No seeds file found, exiting with error if sum file exists"
  exit $([ ! -f "\${sum_path}" ])
fi

set -e
( [ ! -f "\${sum_path}" ] && ( sha1sum "\${seed_path}" > "\${sum_path}" ) ) || sha1sum -c "\${sum_path}"

EOF

COPY --chmod=755 <<EOF /usr/local/sbin/entrypoint.sh
#!/usr/bin/env sh

# Trigger an error if non-zero exit code is encountered
set -e

if [ ! -z "\$AWS_SECRET_ACCESS_KEY" ] && \
   [ ! -z "\$AWS_ACCESS_KEY_ID" ] && \
   [ ! -z "\$AWS_DEFAULT_REGION" ] && \
   [ ! -z "\$AWS_ENDPOINT" ] && \
   [ ! -z "\$S3_SEEDS_URL" ]; then
  echo "S3 env set, fetching seeds def"
  seed_path="/src/assets/data/seeds.json"
  mkdir -p /src/assets/data
  (aws --endpoint-url "\$AWS_ENDPOINT" s3 cp "\$S3_SEEDS_URL" "\$seed_path") || touch "\$seed_path"
fi

envdir -d /env hugo build -d /public --noBuildLock --cacheDir /tmp/cache --cleanDestinationDir --minify

exec caddy run --config /etc/caddy/Caddyfile --adapter caddyfile

EOF


RUN mkdir /env && \
    mkdir /public

COPY . /src

WORKDIR /src

RUN npm ci


ENTRYPOINT [ "/usr/local/sbin/entrypoint.sh" ]
